# Genius Sports Kubernetes Workshop 2019 - Medellin

All resources ( slides, labs etc. ) will be published after the workshop. 

[Join us on Slack #help-workshop](https://join.slack.com/t/gs-nodeconf/shared_invite/enQtNjcwODQyMTY2MjE1LTc4ZTMyOGRiYjM4YzliMWY2MDg5Mjc0NTQ0Yzg0NjRiZGI2M2Y3YzZlNDRjNjljZDlhOWFkODk0OTA0MjVkMzk)

## Workshop Goals
In todays set of labs, we will be attempting to cover the following tasks 
* [Lab 1](lab-1/README.md) - Create and integrate a kubernetes cluster on [GCP](https://cloud.google.com) with [Gitlab](https://gitlab.com/)
* [Lab 2](lab-2/README.md) - Create pipelines to build, test and deploy a set of NodeJS services on Kubernetes
* [Lab 3](lab-3/README.md) - Design some performance tests that can run as part of our pipeline to test capacity in our system
* [Lab 4](lab-4/README.md) - Create some dashboards to visualize the key operational metrics of the services that have been deployed
* [Lab 5](lab-5/README.md) - Learn how to introduce faults into systems in the testing process
* [Lab 6](lab-6/README.md) - Introduce a traffic-shifting approach to production deployments



Survey for after:
https://forms.gle/Ld1mDnGwV4BV5VJ49

Please give us some feedback. We hope you enjoyed this and learnt something.

If you have suggestions please let us know. 